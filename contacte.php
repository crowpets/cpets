<?php
session_start();
?>
<!DOCTYPE html>
<html>

	<!-- Mirrored from envato.megadrupal.com/html/kickstars/contact.html by HTTrack Website Copier/3.x [XR&CO'2013], Thu, 06 Jun 2013 09:23:50 GMT -->
	<head>
		<title>Contacte</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no" />
		<?php include 'assets/blocks_includes/links_head.php'; ?>
		<?php include 'assets/blocks_includes/links/links_contacte.php'; ?>
		
	</head>
	<body>

		<div id="wrapper">
			<?php include 'assets/blocks_includes/header.php'; ?>

			<div class="layout-2cols">
				<div class="content grid_8">
					<div class="single-page">
						<div class="wrapper-box box-post-comment">
							<h2 class="common-title">Contacta'ns</h2>
							<div class="box-white">
								<form id="contact-form" class="clearfix" method="POST">
									<h3>
										Omple el formulari per fer qualsevol consultes, sugerencies o queixes...
									</h3>
									<div class="form form-post-comment">
										<div class="left-input">
											<label for="email_contact">
												<input id="email_contact" type="email" name="email_contact" class="txt fill-width txt-email" placeholder="exemple@exemple.com"/>
											</label>
											<label for="assumpte_contact">
												<input id="assumpte_contact" type="text" name="assumpte_contact" class="txt fill-width txt-email" placeholder="Assumpte"/>
											</label>
											<label for="msn_contact"><textarea name="msn_contact" id="msn_contact" cols="30" rows="10" class="txt fill-width" placeholder="Missatge"></textarea> </label>
										</div>

										<div class="clear"></div>
										<p class="rs ta-r clearfix">
											<span id="response"></span>
											<span class="thanks">Gràcies pels teus sugeriments!<i class="pick-right"></i></span>
											<input type="hidden" name="swiftmailer">
											<button type="submit" id="btn-contact" name="btn-contact" class="btn btn-white btn-submit-comment" >
												<span class="glyphicon glyphicon-log-in"></span> &nbsp; Send
											</button>
										</p>
									</div>
								</form>
								<?php
									//require "assets/send_email/sendmail.php";
								?>
							</div>
						</div><!--end: .box-list-comment -->
					</div>
				</div><!--end: .content -->
				<div class="sidebar grid_4">
					<div class="box-gray">
						<div class="contactImg"><img  src="assets/images/ex/contacte.jpg" alt="" />
						</div>	
						<h3 class="title-box">Informació de contacte</h3>
						<p class="rs pb20">
							<span class="fw-b">Adreça</span>: Carrer: XXX XXX XXX
						</p>
						<p class="rs pb20">
							<span class="fw-b">Telefon</span>: +34 334644345
						</p>
						<p class="rs pb20">
							<span class="fw-b">Email</span>: <a href="mailto:info@megadrupal.com" class="be-fc-orange">info@animalcloser.com</a>
						</p>
						
					</div>
				</div><!--end: .sidebar -->
				<div class="clear"></div>
			</div>
			<?php include 'assets/blocks_includes/footer.php'; ?>

		</div>

		<?php 
			include 'assets/blocks_includes/registrar_login.php';
			include 'assets/blocks_includes/scripts_footer.php';
		?>
	</body>

	<!-- Mirrored from envato.megadrupal.com/html/kickstars/contact.html by HTTrack Website Copier/3.x [XR&CO'2013], Thu, 06 Jun 2013 09:23:52 GMT -->
</html>