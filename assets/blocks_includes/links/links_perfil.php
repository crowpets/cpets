<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
<link href="assets/bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="assets/css/normalize.css"/>
<link rel="stylesheet" href="assets/css/jquery.sidr.light.css"/>
<link rel="stylesheet" href="assets/css/style.css"/>
<!--[if lte IE 7]>
<link rel="stylesheet" href="css/ie7.css"/>
<![endif]-->
<!--[if lte IE 8]>
<link rel="stylesheet" href="css/ie8.css"/>
<![endif]-->
<link rel="stylesheet" href="assets/css/responsive.css"/>
<!--[if lt IE 9]>
<script type="text/javascript" src="js/html5.js"></script>
<![endif]-->
<script type="text/javascript" src="assets/js/login/jquery-1.11.3-jquery.min.js"></script>
<script type="text/javascript" src="assets/js/perfil.js"></script>
<script type="text/javascript" src="assets/js/login/validation.min.js"></script>

<!-- Alerts Personalitzats -->
<link rel="stylesheet" type="text/css" href="assets/alerts/dist/sweetalert.css">
<script src="assets/alerts/dist/sweetalert.min.js"></script>
	
<script type="text/javascript" src="assets/js/raphael-min.js"></script>
<script type="text/javascript" src="assets/js/jquery.sidr.min.js"></script>
<script type="text/javascript" src="assets/js/twitter.min.js"></script>
<script type="text/javascript" src="assets/js/pie.js"></script>
<script type="text/javascript" src="assets/js/script.js"></script>