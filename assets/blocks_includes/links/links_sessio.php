<meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no" />
<!-- Login / Registration -->
<link href="assets/bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="assets/css/normalize.css"/>
<link rel="stylesheet" href="assets/css/jquery.sidr.light.css"/>
<link rel="stylesheet" href="assets/css/style.css"/>
<!--[if lte IE 7]>
<link rel="stylesheet" href="css/ie7.css"/>
<![endif]-->
<!--[if lte IE 8]>
<link rel="stylesheet" href="css/ie8.css"/>
<![endif]-->
<link rel="stylesheet" href="assets/css/responsive.css"/>
<!--[if lt IE 9]>
<script type="text/javascript" src="js/html5.js"></script>
<![endif]-->

<!--JS personalitzat-->
<script type="text/javascript" src="assets/js/myScript.js"></script>
<script type="text/javascript" src="assets/js/raphael-min.js"></script>
<script type="text/javascript" src="assets/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.sidr.min.js"></script>
<script type="text/javascript" src="assets/js/pie.js"></script>
<script type="text/javascript" src="assets/js/script.js"></script>

<!-- JS Login / Registration -->
<script type="text/javascript" src="assets/js/login/jquery-1.11.3-jquery.min.js"></script>
<script type="text/javascript" src="assets/js/login/login.js"></script>
<script type="text/javascript" src="assets/js/login/validation.min.js"></script>

