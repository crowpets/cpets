<!-- Custom CSS -->
    <!--link href="login_connect/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" media="screen"--> 
    <link href="assets/css/agency.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="assets/css/normalize.css"/>
    <link rel="stylesheet" href="assets/css/jquery.sidr.light.css"/>
    <link rel="stylesheet" href="assets/css/animate.min.css"/>
    <link rel="stylesheet" href="assets/css/md-slider.css"/>
    <link rel="stylesheet" href="assets/css/style.css"/>
    

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
    <!--[if lte IE 7]>
    <link rel="stylesheet" href="css/ie7.css"/>
    <![endif]-->
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="css/ie8.css"/>
    <![endif]-->
    <link rel="stylesheet" href="assets/css/responsive.css"/>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="js/html5.js"></script>
    <![endif]-->
    <script type="text/javascript" src="assets/js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="assets/js/raphael-min.js"></script>
    
    
    <script type="text/javascript" src="assets/js/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.touchwipe.min.js"></script>
    <script type="text/javascript" src="assets/js/md_slider.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.sidr.min.js"></script>
    <script type="text/javascript" src="assets/js/pie.js"></script>
    <script type="text/javascript" src="assets/js/script.js"></script>