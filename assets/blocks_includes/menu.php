<div class="header-left">
	<div class="main-nav clearfix">
		<div class="nav-item">
			<a href="categories.php" class="nav-title">Explorar</a>
			<p class="rs nav-description">
				Altres Projectes
			</p>
		</div><!-- end Altres projectes -->
		<span class="sep"></span>
		<div class="nav-item">
			<a href="comenca.php" class="nav-title">Començar</a>
			<p class="rs nav-description">
				El teu projecte
			</p>
		</div><!-- end El teu projecte -->
		<span class="sep"></span>
		<div class="nav-item">
			<a href="contacte.php" class="nav-title">Contacte</a>
			<p class="rs nav-description">
				Contacta'ns
			</p>
		</div><!-- end Contactan's -->
	</div><!-- ./main-nav clearfix -->
</div><!-- ./header-left -->