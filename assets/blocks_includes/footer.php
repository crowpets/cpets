<footer id="footer">	
			<div class="copyright">
				<div class="container_12">
					<div class="grid_12">
						<a class="logo-footer" href="index-2.html"><img src="assets/images/logo-2.png" alt="$SITE_NAME"/></a>
						<p class="rs term-privacy">
							<a class="fw-b be-fc-orange" href="single.html">Termes & Condicions</a>
							<span class="sep">/</span>
							<a class="fw-b be-fc-orange" href="single.html">Drets reservats</a>
							<span class="sep">/</span>
							<a class="fw-b be-fc-orange" href="#">FAQ</a>
						</p>
						<p class="rs ta-c fc-gray-dark site-copyright">
							Designed by PetCloser.
						</p>
					</div>
					<div class="clear"></div>
				</div>
			</div>
</footer><!--end: #footer -->