<?php
	session_start();
	require_once '../config/connect_db.php';
	
	function isUserExisted($db_con,$user_email) {
		try
		{	
		
			$stmt = $db_con->prepare("SELECT * FROM users WHERE user_email = ?");
			$stmt->bindParam(1,$user_email, PDO::PARAM_INT);
			$stmt->execute();
			$row = $stmt->fetch(PDO::FETCH_ASSOC);
			//$count = $stmt->rowCount();
			
			if($row){
				
				return false;
			}
			else{
				
				return true;
			}
				
		}
		catch(PDOException $e){
			echo $e->getMessage();
		}
	}

	if(isset($_POST['btn-login']))
	{
		//$user_name = $_POST['user_name'];
		$user_email = trim($_POST['user_email']);
		$user_password = trim($_POST['password']);
		
		//$password = md5($user_password);
		
		try
		{	
		
			$stmt = $db_con->prepare("SELECT * FROM users WHERE user_email=:email");
			$stmt->execute(array(":email"=>$user_email));
			$row = $stmt->fetch(PDO::FETCH_ASSOC);
			$count = $stmt->rowCount();
			
			if($row['user_password'] == $user_password){
				
				echo "ok"; // log in
				$_SESSION['user_session'] = $row['user_name'];
				$_SESSION['user_session_id'] = $row['user_id'];
				$_SESSION['user_session_email'] = $row['user_email'];
			}
			else{
				
				echo "El email o la contrassenya és incorrecte."; // wrong details 
			}
				
		}
		catch(PDOException $e){
			echo $e->getMessage();
		}
	}
	
	
	if(isset($_POST['btn-register']))
	{
		$userName = $_POST['userName'];
		$userEmail = trim($_POST['userEmail']);
		$userPassword = trim($_POST['userPassword']);
		
		//$password = md5($user_password);
		
		try
		{	
			// Registrem l'usuari
			$stmt = $db_con->prepare("INSERT INTO users(user_name,user_email,user_password) VALUES (:user_name, :user_email, :user_password)");
			$stmt->bindParam(':user_name', $userName);
			$stmt->bindParam(':user_email', $userEmail);
			$stmt->bindParam(':user_password', $userPassword);
			
			$checkUser = isUserExisted($db_con,$userEmail);
			if( $checkUser){
				$check = $stmt->execute();
				if($check){
					echo "ok"; // log in
				}			
			}
			else{				
				echo "L'email ja existeix."; // wrong details 
			}
				
		}
		catch(PDOException $e){
			echo $e->getMessage();
		}
	}
?>