/**
 * @author zhuwei
 */

var counter = 0;
function moreFields() {
	counter++;
	var newFields = document.getElementById('readroot').cloneNode(true);
	newFields.id = '';
	var newField = newFields.getElementsByTagName("INPUT");
	for (var i=0;i<newField.length;i++) {
		var theName = newField[i].name;
		if (theName) {
			newField[i].name = newField[i].name.replace('[0]','')+"["+ counter +"]";
			newField[i].value = "";
		}
	}
	var insertHere = document.getElementById('writeroot');
	$(insertHere).append(newFields);
}

window.onload = function() {
	document.getElementById("bAfegir").addEventListener('click', moreFields, false);
};