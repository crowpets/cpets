$('document').ready(function()
{ 
     /* validation Perfil*/
	 $("#perfil-form").validate({
      rules:
	  {
			nom_perfil: {
			required: true,
			},
			email_perfil: {
            required: true,
            },
			password_perfil: {
            required: true,
            },
			password_perfil_nou: {
            required: true,
			minlength : 5,
            },
	   },
       messages:
	   {
            nom_perfil:{
                      required: "please enter your name"
                     },
            email_perfil: "please enter your email address",
			password_perfil: "please enter your password",
			password_perfil_nou: "please enter your new password",
       },
			submitHandler: submitFormPerfil
       }); 
	   
	   /* login submit */
	   function submitFormPerfil()
	   {		
			var data = $("#perfil-form").serialize();
				
			$.ajax({
			type : 'POST',
			url  : 'assets/perfil_change/get_perfil.php',
			data : data,
			dataType: 'json',
			beforeSend: function()
			{	
				$("#errorPerfil").fadeOut();
				$("#btn-perfil").html('<span class="glyphicon glyphicon-transfer"></span> &nbsp; sending...');
			}
			}).done(function(response)
			   {						
					if(response.msn == "ok"){
						$('.user_name').html(response.nom);
						$('#email_info').html(response.email);
						swal("Has guardat correctament els canvis!", "", "success");
						$("#btn-perfil").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Guardar els canvis');
					}
					else{
						$("#errorPerfil").fadeIn(1000, function(){						
						$("#errorPerfil").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; '+response.msn+' !</div>');
							$("#btn-perfil").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Guardar els canvis');
								});
					}
			}).fail(function() {
				alert( "error ajax" );
			});
				return false;
		}
});