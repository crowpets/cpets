$('document').ready(function()
{ 
	 /* validation contact*/
	  
	 $("#contact-form").validate({
      rules:
	  {
			email_contact: {
				required: true,
			},
			assumpte_contact: {
				required: true,
            },
			msn_contact: {
				required: true,
            },
	   },
       messages:
	   {
            email_contact:{
                      required: "Entra un email"
                     },
            assumpte_contact: "Afegeix un assumpte",
			msn_contact: "Escriu teu missatge",
       },
	   submitHandler: submitFormContact	
       }); 
	   
	   /* login submit */
	   
	   function submitFormContact()
	   {		
			var data = $("#contact-form").serialize();
				
			$.ajax({
			type : 'POST',
			url  : 'assets/send_email/sendmail.php',
			data : data,
			beforeSend: function()
			{	
				$("#btn-contact").html('<span class="glyphicon glyphicon-transfer"></span> &nbsp; sending ...');
			},
			success :  function(response)
			   {						
					if(response=="ok"){
						swal("Missatge enviat!", "", "success");
						$("#btn-contact").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Send');
					}
					else{				
						sweetAlert("Missatge no enviat!", "Alguna dada introduïda és erronea!", "error");
					}
			  }
			});
				return false;
		}
		
});