/*------------------------------------------------------------------------
 # Eloi / Zhuwey - 2016
 # ------------------------------------------------------------------------
 # DAW2-T
 --------------------------------------------------------------------------*/
/* NAMES AND ID'S
categoria_projecte (select)
titol_projecte
pais_projecte (select)
ciutat_projecte
multimedia_projecte
termini_projecte
cost_projecte
tContribucio
tDescripcio
tEntrega
tPadro
tHistoria
*/
$('document').ready(function()
{
/***********************************************************************
***************** CLONACIÓ DE L'APARTAT 'RECOMPENSES' *****************
************************************************************************/


/***********************************************************************
******************** VALIDACIÓ DE L'APARTAT 'BÀSIC' ********************
************************************************************************/
	/* 
	* Assignem a variables bool el nom de cada formulari de recompenses 
	* i el valor false, per indicar que els formularis encara no han estat 
	* estat validats.
	*/
	var basic = false;
	var recompenses = false;
	var historia = false;

	function validarFormBasic() {
		//var categoria = $('#categoria_projecte').val();
		var titol = $('#titol_projecte').val();
		//var pais = $('#pais_projecte').val();
		var ciutat = $('#ciutat_projecte').val();
		var multimedia = $('#multimedia_projecte').val();
		var ext = $('#multimedia_projecte').val().split('.').pop().toLowerCase();
		var file_size = $('#multimedia_projecte').size();
		var termini = $('#termini_projecte').val();
		var cost = $('#cost_projecte').val();
		var not_error = true;

		if(titol === '' || titol === null) {
			$('#titolError').html('<p><i class="fa fa-exclamation-circle"></i> Introdueix un títol al projecte.<br/>[Mínim 6 caràcters]</p><br/>');
			not_error = false;
		} else if(titol.length < 6) {
			$('#titolError').html('<p><i class="fa fa-exclamation-circle"></i> Introdueix un títol al projecte.<br/>[Mínim 6 caràcters]</p><br/>');
			not_error = false;
		}
		if(ciutat === '' || ciutat === null) {
			$('#ciutatError').html('<p><i class="fa fa-exclamation-circle"></i> Introdueix una ciutat.</p><br/>');
			not_error = false;
		} else if(ciutat.length < 3) {
			$('#ciutatError').html('<p><i class="fa fa-exclamation-circle"></i> Introdueix una ciutat.<br/>[Mínim 3 caràcters]</p><br/>');
			not_error = false;		}
		if(multimedia.length <= 0){
			$('#arxiuError').html('<p><i class="fa fa-exclamation-circle"></i> Selecciona un imatge o un video.</p><br/>');
			not_error = false;
		}else if($.inArray(ext, ['mp4','wmv','avi','flv','png','jpg','jpeg']) == -1){
			$('#arxiuError').html('<p><i class="fa fa-exclamation-circle"></i> Format error!</p><br/>');
			not_error = false;
		}else if(file_size > 20971520){
			$('#arxiuError').html('<p><i class="fa fa-exclamation-circle"></i> La mida del fitxer no pot superar a 20MB!</p><br/>');
			not_error = false;
		}
		if(termini === '' || termini === null) {
			$('#terminiError').html('<p><i class="fa fa-exclamation-circle"></i> Introdueix els dies del termini.</p><br/>');
			not_error = false;
		} else if (termini < 30 || termini > 365) {
			$('#terminiError').html('<p><i class="fa fa-exclamation-circle"></i> Introdueix un termini valid!<br/>[Mínim 30 díes, Màxim 365 díes]</p><br/>');
			not_error = false;
		}
		if(cost === '' || cost === null) {
			$('#costError').html('<p><i class="fa fa-exclamation-circle"></i> Introdueix la meta econòmica del projecte.</p>');
			not_error = false;
		} else if (cost < 100 || cost > 100000) {
			$('#costError').html('<p><i class="fa fa-exclamation-circle"></i> Introdueix la meta vàlida!<br/>[Mínim 100€, Màxim 100000€]</p>');
			not_error = false;
		}

		return not_error;		
	}
	// Al treure el focus de cada item
	$('#titol_projecte').keyup(function(){
		var titol = $('#titol_projecte').val();
		if((titol != '' || titol != null) && titol.length >= 6) {
			// Netegem el camp error
			$('#titolError').empty();
		} else {
			$('#titolError').html('<p><i class="fa fa-exclamation-circle"></i> Introdueix un títol al projecte.<br/>[Mínim 6 caràcters]</p><br/>');
			// El formaulari encara no està validat correctament
			basic = false;
		}
	});
	$('#ciutat_projecte').keyup(function(){
		var ciutat = $('#ciutat_projecte').val();
		if((ciutat != '' || ciutat != null) && ciutat.length >= 3) {
			// Netegem el camp error
			$('#ciutatError').empty();
		} else {
			$('#ciutatError').html('<p><i class="fa fa-exclamation-circle"></i> Introdueix una ciutat.<br/>[Mínim 3 caràcters]</p><br/>');
			// El formaulari encara no està validat correctament
			basic = false;
		}
	});
	$('#multimedia_projecte').change(function(){
		var multimedia = $('#multimedia_projecte').val();
		var ext = $('#multimedia_projecte').val().split('.').pop().toLowerCase();
		var file_size = $('#multimedia_projecte')[0].files[0].size;
		if((multimedia != '' || multimedia != null) && multimedia.length >= 1 && $.inArray(ext, ['gif','png','jpg','jpeg']) != -1 && file_size < 20971520) {
			// Netegem el camp error
			$('#arxiuError').empty();
		} else {
			$('#arxiuError').html('<p><i class="fa fa-exclamation-circle"></i> Selecciona un imatge o video(la mida no pot superar a 20MB)</p><br/>');
			// El formaulari encara no està validat correctament
			basic = false;
		}
	});
	$('#termini_projecte').keyup(function(){
		var termini = $('#termini_projecte').val();
		if((termini != '' || termini != null) && (termini >= 30 && termini <= 365)) {
			// Netegem el camp error
			$('#terminiError').empty();
		} else {
			$('#terminiError').html('<p><i class="fa fa-exclamation-circle"></i> Introdueix els dies del termini.</p><br/>');
			// El formaulari encara no està validat correctament
			basic = false;
		}
	});
	$('#cost_projecte').keyup(function(){
		var cost = $('#cost_projecte').val();
		if(cost != '' || cost != null) {
			if (cost < 100 || cost > 100000) {
				$('#costError').html('<p><i class="fa fa-exclamation-circle"></i> Introdueix una meta vàlida!<br/>[Mínim 100€, Màxim 100000€]</p>');
				basic = false;
			} else {
				// Netegem el camp error
				$('#costError').empty();
			}
		} else {
			$('#costError').html('<p><i class="fa fa-exclamation-circle"></i> Introdueix una meta vàlida!<br/>[Mínim 100€, Màxim 100000€]</p>');
			basic = false;
		}
	});


	// Boto 1 següent
	$("#bSeg").click(function() {
		
		if(validarFormBasic()) {
			// Ocultem l'avis d'errors del formulari
			$('#basicError').css({
				display: 'none'
			});
			
			// Avisem que el formulari s'ha validat correctament
			basic = true;			
			// Si no existeix cap error, guardem les dades i passem a la següent apartat
			$(".nav-tabs > li").eq(0).removeClass("active");
			$(".nav-tabs > li").eq(1).addClass("active");
			$(".nav-tabs > li").parents(".tabbable").find(".tab-content .tab-pane").removeClass("active").eq(1).addClass("active");
		}
	});
	/* 
	* Amb aquesta funció, anulem la pestanya 'Recompenses' si el formulari 
	* 'Bàsic' no ha estat validat.
	*/
	$('#recompenses_apartat').click(function(){
		if(!basic) {
			// Mostrem el missatge d'error
			$('#basicError').css({
				display: 'inline-block'
			});
			// Validem el formulari
			validarFormBasic();
			// Barem l'execució
			return false;
		} else {
			// Continua l'execució al apartat 'Recompenses'
			return true;
		}
	}); // end validació pestanya recompenses

	$('#historia_apartat').click(function(){
		if(!validarFormBasic() || !validFormRecompenses()) {
			// Si el formulari actiu és el Bàsic, mostrem el seu missatge d'error
			if(!basic) {
				// Mostrem el missatge d'error
				$('#basicError').css({
					display: 'inline-block'
				});
			}
			if(!recompenses) {
				// Mostrem el missatge d'error
				$('#recompensesError').css({
					display: 'inline-block'
				});
			}
			// Parem l'execució (Evitem que salti de formulari/apartat)
			return false;
			
		} else {
			// Continua l'execució al apartat 'Recompenses'
			return true;
		}
	}); // end validació pestanya història
/*######################################################################
######################## FI DE L'APARTAT 'BÀSIC' #######################
######################################################################*/

/***********************************************************************
***************** VALIDACIÓ DE L'APARTAT 'RECOMPENSES' *****************
************************************************************************/
	// Validació del formulari recompenses
	function validFormRecompenses() {
		// Marquem el formulari com ha no validat
		recompenses = false;
		// Assignem true, conforme no hi ha cap error
		var not_error = true;
		// Validem les contribucions
		$(function(){
	        $('.recompensa-contribucio').each(function(){
	            var valor = $(this).val();
	            if(valor < 5) {
	            	$(this).css({
	            		border: '1px solid #ea503d'
	            	});
	            	not_error = false;
	            } else {
	            	$(this).css({
	            		border: '1px solid #D9D9D9'
	            	});
	            }
	        });
	    });
		// Validem la descripció
	    $(function(){
	        $('.recompensa-descripcio').each(function(){
	        	var valor = $(this).val();
	            if((valor === '' || valor === null) || valor.length < 20) {
	            	$(this).css({
	            		border: '1px solid #ea503d'
	            	});
	            	not_error = false;
	            } else {
	            	$(this).css({
	            		border: '1px solid #D9D9D9'
	            	});
	            }
	        });
	    });
	    function validDate(valor) {
	    	//var entrega = valor.split("/");
        	var data_davui = new Date();
			var data_usuari = new Date(valor);
			
        	// Si la data és inferior a la data actual
        	if(data_usuari <= data_davui) {
        		$(this).css({
        			border: '1px solid #ea503d'
            	});
            	not_error = false;
        	} else {
        		// Eliminem el format d'error
        		$(this).css({
        			border: '1px solid #D9D9D9'
        		});
        	} 
	    }
	    // Validem la data
	    $(function(){
	        $('.recompensa-entregaestimada').each(function(){
	        	validDate($(this).value);
	        });
	    });
	    // Validem els patrocinadors
	    $(function(){
	        $('.recompensa-patrocinadors').each(function(){
	        	var valor = $(this).val();
	            if(valor < 1) {
	            	$(this).css({
	            		border: '1px solid #ea503d'
	            	});
	            	not_error = false;
	            } else {
	            	$(this).css({
	            		border: '1px solid #D9D9D9'
	            	});
	            }
	        });
	    });

	    // Si existeix algun error
	    if(!not_error) {
	    	$('#recompensesError').css({
				display: 'inline-block'
			});
	    	recompenses = false;
	    } else {
	    	// Mostrem el missatge d'error
			$('#recompensesError').css({
				display: 'none'
			});
	    	recompenses = true;
	    }
	    return not_error;
	}

	// Boto 2 Següent
	$("#bSeg2").click(function() {
		if(validFormRecompenses()) {
			$(".nav-tabs > li").eq(1).removeClass("active");
			$(".nav-tabs > li").eq(2).addClass("active");
			$(".nav-tabs > li").parents(".tabbable").find(".tab-content .tab-pane").removeClass("active").eq(2).addClass("active");
		}
	});
	// Validacions individuals per cada camp
	/*
	* És necessari per si un usuari retrocedeix en un 
	* altre formulari ja reomplert i modifica alguna dada 
	*/
	$('.recompensa-contribucio').keyup(function(){
		$(function(){
	        $('.recompensa-contribucio').each(function(){
	            var valor = $(this).val();
	            if(valor < 5) {
	            	$(this).css({
	            		border: '1px solid #ea503d'
	            	});
	            	not_error = false;
	            } else {
	            	$(this).css({
	            		border: '1px solid #D9D9D9'
	            	});
	            }
	        });
	    });
	}); // end .recompensa-contribucio (keyup)

	$('.recompensa-descripcio').keyup(function(){
		$('.recompensa-descripcio').each(function(){
        	var valor = $(this).val();
            if((valor === '' || valor === null) || valor.length < 20) {
            	$(this).css({
            		border: '1px solid #ea503d'
            	});
            	recompenses = false;
            } else {
            	$(this).css({
            		border: '1px solid #D9D9D9'
            	});
            }
        });
	}); // end .recompensa-descripcio (keyup)

	$('.recompensa-entregaestimada').keyup(function(){
		$(function(){
	        $('.recompensa-entregaestimada').each(function(){
	        	validDate($(this).value);
	        });
	    });
	}); // end .recompensa-entregaestimada (keyup)

	$('.recompensa-patrocinadors').keyup(function(){
		$(function(){
	        $('.recompensa-patrocinadors').each(function(){
	        	var valor = $(this).val();
	            if(valor < 1) {
	            	$(this).css({
	            		border: '1px solid #ea503d'
	            	});
	            	recompenses = false;
	            } else {
	            	$(this).css({
	            		border: '1px solid #D9D9D9'
	            	});
	            }
	        });
	    });
	}); // end .recompensa-patrocinadors (keyup)
/*######################################################################
######################## FI DE L'APARTAT 'RECOMPENSES' #######################
######################################################################*/

/***********************************************************************
***************** VALIDACIÓ DE L'APARTAT 'HISTÒRIA' *****************
************************************************************************/
/*
	$("#categoria").click(function(){
		console.log(JSON.parse(localStorage.getItem("recompenses")));
		return false;
	});
*/
	function validFormHistoria() {
		var historia = $('.historia').val();
		if(historia.length < 250) {
			$('.historia').css({
	            border: '1px solid #ea503d'
	        });
			$('#historiaError').css({
				display: 'inline-block'
			});
			return false;
		} else {
			$('#historiaError').css({
				display: 'none'
			});
			return true;
		}
	}
	
	// Function to preview image after validation
	$(function() {
		$("#multimedia_projecte").change(function() {
		var file = this.files[0];
		var imagefile = file.type;
		var reader = new FileReader();
		reader.onload = imageIsLoaded;
		reader.readAsDataURL(this.files[0]);
	});
	function imageIsLoaded(e) {
		$("#file").css("color","green");
		$('#image_preview').css("display", "block");
		$('#previewing').attr('src', e.target.result);
		$('#previewing').attr('width', '250px');
		$('#previewing').attr('height', '230px');
		};
	});
	// Submit, si tot és correcte passem a AJAX
	$( "#formCampanya" ).on( "submit", function( event ) {
	  	event.preventDefault();
	  	if(validFormHistoria()) {
			//var multimedia = $('#multimedia_projecte').val();
			//var filename = multimedia.replace(/^.*\\/, "");
  			console.log( new FormData(this));
  			$.ajax({
				type:'POST',
                url:'assets/project/create_project.php',
                data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
				contentType: false,       // The content type used when sending data to the server.
				cache: false,             // To unable request pages to be cached
				processData:false,        // To send DOMDocument or non processed data file it is set to false
                success:function(result){
                    console.log(result);
                }
            });
		}	
	});
});