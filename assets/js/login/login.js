$('document').ready(function()
{ 
     /* validation Login*/
	 $("#login-form").validate({
      rules:
	  {
			password: {
			required: true,
			
			},
			user_email: {
            required: true,
            },
	   },
       messages:
	   {
            password:{
                      required: "please enter your password"
                     },
            user_email: "please enter your email address",
       },
	   submitHandler: submitFormLogin	
       }); 
	
	   
	   /* validation Register*/
	    $("#register-form").validate({
		  rules:
		  {		
				userName: {
				required: true,
				},
				userPassword: {
				required: true,
				minlength : 5,
				},
				userPassword_confirm : {
					required: true,
                    minlength : 5,
                    equalTo : "#userPassword"
                },
				userEmail: {
				required: true,
				},
				userEmail_confirm : {
					required: true,
                    minlength : 5,
                    equalTo : "#userEmail"
                },
		   },
			messages:
		   {
			    userName: "please enter your name",
				userPassword:{
						  required: "please enter your password"
						 },
				userPassword_confirm:{
						  required: "please enter your password"
						 },
				userEmail_confirm:{
						  required: "please enter your password"
						 },		 
				//userEmail: "please enter your email address",
		   },
	   submitHandler: submitFormRegister	
       }); 
	   
	   
	   /* login submit */
	   function submitFormLogin()
	   {		
			var data = $("#login-form").serialize();
				
			$.ajax({
			type : 'POST',
			url  : 'assets/login_connect/login_process.php',
			data : data,
			beforeSend: function()
			{	
				$("#errorLogin").fadeOut();
				$("#btn-login").html('<span class="glyphicon glyphicon-transfer"></span> &nbsp; sending ...');
			},
			success :  function(response)
			   {						
					if(response=="ok"){
									
						$("#btn-login").html('<img src="assets/images/ajax-loader.gif" /> &nbsp; Signing In ...');
						setTimeout(' window.location.href = "index.php"; ',4000);
					}
					else{
									
						$("#errorLogin").fadeIn(1000, function(){						
						$("#errorLogin").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; '+response+' !</div>');
											$("#btn-login").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Sign In');
									});
					}
			  }
			});
				return false;
		}
	   /* login submit */
	   
	   /* Register submit */
	   function submitFormRegister()
	   {		
			var data = $("#register-form").serialize();
				
			$.ajax({
			type : 'POST',
			url  : 'assets/login_connect/login_process.php',
			data : data,
			beforeSend: function()
			{	
				$("#prompt").fadeOut();
				$("#btn-register").html('<span class="glyphicon glyphicon-transfer"></span> &nbsp; sending ...');
			},
			success :  function(response)
			   {						
					if(response=="ok"){	
						$("#btn-register").html('<img src="assets/images/ajax-loader.gif" /> &nbsp; Signing In ...');
						$("#prompt").fadeIn(1000, function(){
							// Netegem els camps del formulari
							$('#userName').val("");
							$('#userEmail').val("");
							$('#userEmail_confirm').val("");
							$('#userPassword').val("");
							$('#userPassword_confirm').val("");
							// Llancem el missatge de registre					
							$("#prompt").html('<div class="alert alert-success"> <span class="glyphicon glyphicon-ok"></span> &nbsp; Usuari registrat correctament!<p>Ja pots iniciar sessió!</p></div>');
							$("#btn-register").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Sign In');
						});
					}
					else{
									
						$("#prompt").fadeIn(1000, function(){						
							$("#prompt").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; '+response+' !</div>');
							$("#btn-register").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Sign In');
						});
					}
			  }
			});
				return false;
		}
	   /* Register submit */
});