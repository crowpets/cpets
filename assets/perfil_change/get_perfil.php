<?php
	session_start();
	require_once '../config/connect_db.php';

	function comprovarContrassenya($db_con,$password_perfil) {
		try
		{	
		
			$stmt = $db_con->prepare("SELECT * FROM users WHERE user_password = ?");
			$stmt->bindParam(1,$password_perfil, PDO::PARAM_INT);
			$stmt->execute();
			$row = $stmt->fetch(PDO::FETCH_ASSOC);
			//$count = $stmt->rowCount();
			
			if($row){
				
				return true;
			}
			else{
				
				return false;
			}
				
		}
		catch(PDOException $e){
			echo $e->getMessage();
		}	
	}
	
	if(isset($_SESSION['user_session']) && isset($_POST['btn-perfil']))
	{
		$nom_perfil = $_POST['nom_perfil'];
		$email_perfil = trim($_POST['email_perfil']);
		$password_perfil = trim($_POST['password_perfil']);
		$password_perfil_nou = trim($_POST['password_perfil_nou']);
		//$password = md5($user_password);
		
		try
		{	
			
			$stmt = $db_con->prepare("UPDATE users SET user_name = :user_name, user_email = :user_email ,user_password = :user_password WHERE user_id = ".$_SESSION['user_session_id']);
			$stmt->bindParam(':user_name', $nom_perfil);
			$stmt->bindParam(':user_email', $email_perfil);
			$stmt->bindParam(':user_password', $password_perfil_nou);
			$checkPassword = comprovarContrassenya($db_con,$password_perfil);
			if( $checkPassword){
				$check = $stmt->execute();
				if($check){
					$_SESSION['user_session'] = $nom_perfil;
					$_SESSION['user_session_email'] = $email_perfil;

					$data = array(
						'msn' => 'ok',
						'nom' => $nom_perfil,
						'email' => $email_perfil
					); // log in
				}else{
					$data = array('msn' => "Ha passat un error durant la actualització!");
				}			
			}
			else{				
				$data = array('msn' => "La teva contrasenya no es correcte!"); // wrong details 
			}
			echo json_encode($data);
		}
		catch(PDOException $e){
			echo $e->getMessage();
		}
	}
?>