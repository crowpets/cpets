<?php

	// Estem en sessio
	session_start();
	// Connectem en la Base de dades
	include '../config/connect_db.php';

		
	if(isset($_POST['is_user']))
	{
		/**************************************
		*** RECOLLIM LES DADES DEL PROJECTE ***
		**************************************/
		// APARTAT BASIC
		$titol = trim($_POST['titol_projecte']);
		$categoria = trim($_POST['categoria_projecte']);
		$pais =  trim($_POST['pais_projecte']);
		$ciutat =  trim($_POST['ciutat_projecte']);
		$termini =  trim($_POST['termini_projecte']);
		$cost =  trim($_POST['cost_projecte']);
		// Apartat Recompenses (Arrays)
		$contribucio = $_POST['tContribucio'];
		$descripcio = $_POST['tDescripcio'];
		$entregar = $_POST['tEntrega'];
		$patrocinadors = $_POST['tPatrocinadors'];
		// Apartat Història
		$historia = $_POST['tHistoria'];
		// Adquirim el número de recompenses
		$num_recompenses = count($contribucio);
		
		// ID del usuari logat
		$user_id = $_SESSION['user_session_id'];
		$check = "";
		$FileUploadscheck = "";
		//Upload files
		if(isset($_FILES["multimedia_projecte"]["type"])){
			$validextensions = array("jpeg", "jpg", "png");
			$temporary = explode(".", $_FILES["multimedia_projecte"]["name"]);
			$file_extension = end($temporary);
			if ((($_FILES["multimedia_projecte"]["type"] == "image/png") || ($_FILES["multimedia_projecte"]["type"] == "image/jpg") || ($_FILES["multimedia_projecte"]["type"] == "image/jpeg")
				) && ($_FILES["multimedia_projecte"]["size"] < 10000000)//Approx. 100kb files can be uploaded.
				&& in_array($file_extension, $validextensions)) {
			if ($_FILES["multimedia_projecte"]["error"] > 0){
				echo "Return Code: " . $_FILES["multimedia_projecte"]["error"] . "<br/><br/>";
			}
			else
			{
			if (file_exists("../../uploads_users/" . $_FILES["multimedia_projecte"]["name"])) {
				echo $_FILES["multimedia_projecte"]["name"] . " <span id='invalid'><b>already exists.</b></span> ";
			}
			else{
				$sourcePath = $_FILES['multimedia_projecte']['tmp_name']; // Storing source path of the file in a variable
				$targetPath = "../../uploads_users/".$_FILES['multimedia_projecte']['name']; // Target path where file is to be stored
				$FileUploadscheck = move_uploaded_file($sourcePath,$targetPath) ; // Moving Uploaded file
				echo "<span id='success'>Image Uploaded Successfully...!!</span><br/>";
				echo "<br/><b>File Name:</b> " . $_FILES["multimedia_projecte"]["name"] . "<br>";
				echo "<b>Type:</b> " . $_FILES["multimedia_projecte"]["type"] . "<br>";
				echo "<b>Size:</b> " . ($_FILES["multimedia_projecte"]["size"] / 1024) . " kB<br>";
				echo "<b>Temp file:</b> " . $_FILES["multimedia_projecte"]["tmp_name"] . "<br>";
				}
			}
			}
			else{
				echo "<span id='invalid'>***Invalid file Size or Type***<span>";
			}
		}
		try
		{	
			// Guardem el projecte
			$projecte = $db_con->prepare("INSERT INTO project(user_id,categoria,titol,pais,ciutat,multimedia,termini,cost_projecte,historia) VALUES (:user_id,:categoria,:titol,:pais,:ciutat,:multimedia,:termini,:cost_projecte,:historia)");
			$projecte->bindParam(':user_id', $user_id);
			$projecte->bindParam(':categoria', $categoria);
			$projecte->bindParam(':titol', $titol);
			$projecte->bindParam(':pais', $pais);
			$projecte->bindParam(':ciutat', $ciutat);
			$projecte->bindParam(':multimedia',$_FILES["multimedia_projecte"]["name"]);
			$projecte->bindParam(':termini', $termini);
			$projecte->bindParam(':cost_projecte', $cost);
			$projecte->bindParam(':historia', $historia);
			
			$projecte->execute();

			// Recollim la ID del projecte
			$stmt = $db_con->prepare("SELECT id 
				FROM project 
				WHERE user_id= ".$user_id."
				ORDER BY id DESC
				LIMIT 1");
			$stmt->execute();
			$row = $stmt->fetch(PDO::FETCH_ASSOC);
			// -- Guardem la id
			$id_projecte = $row['id'];

			// Guardem les recompenses
			for($reco = 0; $reco < $num_recompenses; $reco++) {
				$save_recompensa = $db_con->prepare("INSERT INTO recompenses(id_projecte,contribucio,descripcio,entrega_estimada,patrocinador) VALUES (:id_projecte,:contribucio,:descripcio,:entrega_estimada,:patrocinador)");
				$save_recompensa->bindParam(':id_projecte', $id_projecte);
				$save_recompensa->bindParam(':contribucio', $contribucio[$reco]);
				$save_recompensa->bindParam(':descripcio', $descripcio[$reco]);
				$save_recompensa->bindParam(':entrega_estimada', $entregar[$reco]);
				$save_recompensa->bindParam(':patrocinador', $patrocinadors[$reco]);
				$check = $save_recompensa->execute();
			}
			//$sourcePath = $_FILES['multimedia_projecte']['tmp_name']; // Storing source path of the file in a variable
			//$targetPath = "/cpets/uploads_users/".$_FILES['multimedia_projecte']['name']; // Target path where file is to be stored
			//move_uploaded_file($sourcePath,$targetPath) ; // Moving Uploaded file
			if($check == 1 && $FileUploadscheck ){
				echo "El projecte ha creat correctament!";
			}else{
				echo 'El projecte no ha creat correctament!';
			}			
		}
		catch(PDOException $e){
			echo $e->getMessage();
		}
	}

?>

