<?php

include 'assets/config/connect_db.php';
// ---- Obtenir un PROJECTE
function getProject($db_con, $id_project) {
	$stmt = $db_con->prepare("SELECT * 
		FROM project p JOIN users u WHERE p.user_id = u.user_id AND id= :id");
	$stmt->bindParam(':id',$id_project, PDO::PARAM_INT);
	$stmt->execute();
	$row = $stmt->fetch(PDO::FETCH_ASSOC);

	return $row;	
}
// ---- Obtenir recompenses d'un PROJECTE
function getRecompenses($db_con, $id_project) {
	$stmt = $db_con->prepare("SELECT * 
		FROM recompenses WHERE id_projecte = :id");
	$stmt->bindParam(':id',$id_project, PDO::PARAM_INT);
	$stmt->execute();
	$result = $stmt->fetchAll();

	return (object) $result;	
}


// ---- Obtenir tots els PROJECTES
function getProjectAll($db_con) {

	$stmt = $db_con->prepare('SELECT * FROM project p JOIN users u WHERE p.user_id = u.user_id ORDER BY id');
    $stmt->execute();
    $result = $stmt->fetchAll();
	return (object) $result;
}


// ---- Obtenir els projectes d'una categoria
function getProjectsByCategory($db_con, $categoria) {
	try	{	
		$stmt = $db_con->prepare("SELECT * FROM project p JOIN users u WHERE p.user_id = u.user_id AND categoria = :categoria");
		$stmt->bindParam(":categoria", $categoria, PDO::PARAM_STR, 30);
	    $stmt->execute();
	    $result = $stmt->fetchAll();
		return (object) $result;
	}
	catch(PDOException $e){
		echo $e->getMessage();
	}
}



/*
public function getProgectMoreDonations($db_con) {
	$stmt = $db_con->prepare("SELECT * 
		FROM project
		ORDER BY clics ASC");
	$stmt->execute();
	$row = $stmt->fetch(PDO::FETCH_ASSOC);
}
*/

?>