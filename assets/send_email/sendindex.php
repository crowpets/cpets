<!-- Alerts Personalitzats -->
  <link rel="stylesheet" type="text/css" href="alerts/dist/sweetalert.css">
  <script src="alerts/dist/sweetalert.min.js"></script>
<!--/ Alerts Personalitzats -->
<?php
	require "lib/swift_required.php";

	// Configuración
$transport = Swift_SmtpTransport::newInstance()
->setHost('smtp.gmail.com')
->setPort('587')
->setEncryption('tls')
->setUsername('websinestesic@gmail.com')
->setPassword('SomSinestesicsComCal');

$mailer = Swift_Mailer::newInstance($transport);

// Si el formulario es enviado
if (isset($_POST["swiftmailer"]))
{
	// Recollim les dades
	$nom = $_POST['nom'];
	$email = $_POST['email'];
	$assumpte = $_POST['assumpte'];
	$missatge = $_POST['msn'];
	// Recollim l'idioma
	if(isset($_POST['lang'])) {
		$lang = $_POST['lang'];
	}
	
	$msn = 'Nom: '.$nom.'<br>'.'Email: '.$email.'<br>';
	if(isset($assumpte) & $assumpte!='') {
		$msn = $msn.'Assumpte: '.$assumpte.'<br>';
	}
	if(isset($missatge) & $missatge!='') {
		$msn = $msn.'Missatge: '.$missatge.'<br>';
	}

	// Crear el mensaje
	$message = Swift_Message::newInstance()
	  // Asunto
	  ->setSubject('Missatge de Sinestesic.com')
	  // Remitente
	  ->setFrom(array('websinestesic@gmail.com' => 'Formulari aprendrexines.com'))
	  // Destinatario/s
	  ->setTo(array('eloicolomsalvans@gmail.com' => 'Administrador'))
	  // Body del mensaje
	  ->setBody($msn, 'text/html');

		// Enviar el mensaje
		if ($mailer->send($message))
		{	
			echo '<script type="text/javascript">swal("Missatge enviat!", "", "success");</script>';	 
		}
		else
		{
			echo '<script type="text/javascript">sweetAlert("Missatge no enviat!", "Alguna dada introduïda és erronea!", "error");</script>';
		}
}
?>
