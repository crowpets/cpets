<?php
session_start();
?>
<!DOCTYPE html>
<html>

	<!-- Mirrored from envato.megadrupal.com/html/kickstars/ by HTTrack Website Copier/3.x [XR&CO'2013], Thu, 06 Jun 2013 09:21:02 GMT -->
	<head>
		<title>Pet Closer</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no" />
		<?php include 'assets/blocks_includes/links_head.php'; ?>
		<?php include 'assets/blocks_includes/links/links_categories.php'; ?>
	</head>
	<body>
		<?php include 'assets/project/get_projects.php' ?>
		<div id="wrapper">
			<?php include 'assets/blocks_includes/header.php'; ?>

			<div class="layout-2cols">
				<div class="content grid_9">
					<div class="search-result-page">
						
						<?php
							if(isset($_GET['cat'])) {
								$cat = $_GET['cat'];
							} else {
								$cat = 'Tecnologies';
							}
							
							$tecnologies = getProjectsByCategory($db_con, 'tecnologies');
							$disseny = getProjectsByCategory($db_con, 'disseny');
							$ajuda = getProjectsByCategory($db_con, 'ajuda');
							$seleccionat = getProjectsByCategory($db_con, $cat); 
						?>
						<div class="top-lbl-val">
							<h3 class="common-title">Explorar / 
							<span class="fc-orange">
								<?php echo ucwords($cat); ?>
							</span></h3>
							<div class="count-result">
								<span class="fw-b fc-black">560</span> projects found in this category
							</div>
						</div>
						<div class="list-project-in-category">
							<div class="lbl-type clearfix">
								<h4 class="rs title-lbl"><a href="#" class="be-fc-orange">Staff picks</a></h4>
								<a href="#" class="view-all be-fc-orange">View all</a>
							</div>
							<div class="list-project">
					<?php
					foreach ($seleccionat as $row) {
						echo '<div class="grid_3">
						<div class="project-short sml-thumb">
							<div class="top-project-info">
								<div class="content-info-short clearfix">
									<a href="project.php?project_id='.$row["id"].'" class="thumb-img"> <img src="uploads_users/'.$row["multimedia"].'" alt="$TITLE"> </a>
									<div class="wrap-short-detail">
										<h3 class="rs acticle-title"><a class="be-fc-orange" href="project.php">'.$row["titol"].'</a></h3>
										<p class="rs tiny-desc">
											by <a href="profile.html" class="fw-b fc-gray be-fc-orange">'.$row["user_name"].'</a>
										</p>
										<p class="rs title-description">
											'.mb_strimwidth($row["historia"], 0, 200, "...").'
										</p>
										<p class="rs project-location">
											<i class="icon iLocation"></i>
											'.$row["ciutat"].'
										</p>
									</div>
								</div>
							</div>
							<div class="bottom-project-info clearfix">
								<div class="line-progress">
									<div class="bg-progress">
										<span  style="width: 50%"></span>
									</div>
								</div>
								<div class="group-fee clearfix">
									<div class="fee-item">
										<p class="rs lbl">
											Funded
										</p>
										<span class="val">50%</span>
									</div>
									<div class="sep"></div>
									<div class="fee-item">
										<p class="rs lbl">
											Pledged
										</p>
										<span class="val">$38,000</span>
									</div>
									<div class="sep"></div>
									<div class="fee-item">
										<p class="rs lbl">
											Days Left
										</p>
										<span class="val">25</span>
									</div>
								</div>
							</div>
						</div>
					</div><!--end: .grid_3 > .project-short-->';
					}
				?>
								<div class="clear"></div>
							</div>
						</div><!--end: .list-project-in-category -->
						
						
					</div><!--end: .search-result-page -->
				</div><!--end: .content -->
				<div class="sidebar grid_3">

					<div class="left-list-category">
						<h4 class="rs title-nav">Categòria</h4>
						<ul class="rs nav nav-category">
							<li>
								<a href="categories.php?cat=tecnologies"> Tecnològies <span class="count-val">(<?php echo count((array)$tecnologies);?>)</span> <i class="icon iPlugGray"></i> </a>
							</li>
							<li>
								<a href="categories.php?cat=disseny"> Dissenys <span class="count-val">(<?php echo count((array)$disseny);?>)</span> <i class="icon iPlugGray"></i> </a>
							</li>
							<li>
								<a href="categories.php?cat=disseny"> Ajuda <span class="count-val">(<?php echo count((array)$ajuda);?>)</span> <i class="icon iPlugGray"></i> </a>
							</li>

						</ul>
					</div><!--end: .left-list-category -->
				</div><!--end: .sidebar -->
				<div class="clear"></div>
			</div>
			<?php include 'assets/blocks_includes/footer.php'; ?>

		</div>

		<?php 
			include 'assets/blocks_includes/registrar_login.php';
			include 'assets/blocks_includes/scripts_footer.php';
		?>
	</body>

	<!-- Mirrored from envato.megadrupal.com/html/kickstars/category.html by HTTrack Website Copier/3.x [XR&CO'2013], Thu, 06 Jun 2013 09:23:52 GMT -->
</html>