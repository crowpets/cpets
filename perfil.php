<?php
session_start();
if(!isset($_SESSION['user_session'])){
    header("location:sessio.php");
}
?>
<!DOCTYPE html>
<html>

<!-- Mirrored from envato.megadrupal.com/html/kickstars/profile.html by HTTrack Website Copier/3.x [XR&CO'2013], Thu, 06 Jun 2013 09:24:22 GMT -->
<head>
    <title>Pet Closer</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no" />
    <?php include 'assets/blocks_includes/links_head.php'; ?>
    <?php include 'assets/blocks_includes/links/links_perfil.php' ?>

</head>
<body>
<div id="wrapper">

    <?php include 'assets/blocks_includes/header.php' ?>
    <div class="layout-2cols">
        <div class="content grid_8">
            <div class="project-detail">
                <div class="project-tab-detail tabbable accordion">
                    <ul class="nav nav-tabs clearfix">
                      <li class="active"><a href="#">Perfil</a></li>
                    </ul>

                    <div class="tab-content">
                        <div>
                            <h3 class="rs alternate-tab accordion-label">Profile</h3>
                            <div class="tab-pane accordion-content active">
								<div id="errorPerfil">
									<!-- error will be shown here ! -->
								</div>
                                <div class="form form-profile">
                                    <form method="POST" id="perfil-form">
	
                                        <div class="row-item clearfix">
                                            <label class="lbl" for="nom">Nom complet: </label>
                                            <div class="val">
                                                <input class="txt" type="text" name="nom_perfil" id="nom_perfil">
                                            </div>
                                        </div>
                                        
                                        <div class="row-item clearfix">
                                            <label class="lbl" for="txt_time_zone">Email: </label>
                                            <div class="val">
                                                <input class="txt" type="email" name="email_perfil" id="email_perfil" value="">
                                            </div>
                                        </div>
                                        <div class="row-item clearfix">
                                            <label class="lbl" for="txt_time_zone">Contrasenya: </label>
                                            <div class="val">
                                                <input class="txt" type="password" name="password_perfil" id="password_perfil" value="">
                                            </div>
                                        </div>
                                        <div class="row-item clearfix">
                                            <label class="lbl" for="txt_time_zone">Nova contrasenya: </label>
                                            <div class="val">
                                                <input class="txt" type="password" name="password_perfil_nou" id="password_perfil_nou" value="">
                                            </div>
                                        </div>
                                    
                                        <button class="btn btn-red btn-submit-all" id="btn-perfil" name="btn-perfil" type="submit">
                                            <span class="glyphicon glyphicon-log-in"></span> &nbsp; Guardar els canvis
                                        </button>

                                    </form>
                                </div>
                            </div><!--end: .tab-pane -->
                        </div>
                        
                      </div><!--end: .tab-content -->
                </div><!--end: .project-tab-detail -->
            </div>
        </div><!--end: .content -->
        <div class="sidebar grid_4">
            <div class="box-gray project-author">
                <h3 class="title-box">Detall d'usuari: </h3>
                <div class="media">
                    <div class="media-body">
                        <h4 class="rs pb10"><a href="#" class="be-fc-orange fw-b">
                        Nom:&nbsp;<span class="user_name"><?php echo $_SESSION['user_session'] ?></span></a></h4>
                        <p class="rs">Email:&nbsp;<span id="email_info"><?php echo $_SESSION['user_session_email'] ?></span></p>
                    </div>
                </div>
            </div><!--end: .project-author -->
        </div><!--end: .sidebar -->
        <div class="clear"></div>
    </div>
    <br>
    <?php include 'assets/blocks_includes/footer.php'; ?>

</div>
</body>

<!-- Mirrored from envato.megadrupal.com/html/kickstars/profile.html by HTTrack Website Copier/3.x [XR&CO'2013], Thu, 06 Jun 2013 09:24:25 GMT -->
</html>