<?php
session_start();
?>
<!DOCTYPE html>
<html>

	<!-- Mirrored from envato.megadrupal.com/html/kickstars/ by HTTrack Website Copier/3.x [XR&CO'2013], Thu, 06 Jun 2013 09:21:02 GMT -->
	<head>
		<title>Pet Closer</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no" />
		<?php include 'assets/blocks_includes/links_head.php'; ?>
		<?php include 'assets/blocks_includes/links/links_sessio.php'; ?>
	</head>
	<body class="sessio">

		<div id="wrapper">
			<?php include 'assets/blocks_includes/header.php'; ?>
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-md-6">
						<div class="form login-form">
								<form method="post" id="register-form">
								<h3 class="rs title-form">Register-se</h3>
								<div class="box-white">
								<div id="prompt">
										<!-- error will be shown here ! -->
								</div>
									<h4 class="rs title-box">Ets nou ha Pet Closer?</h4>
									<p class="rs">
										Crea un compte ha PetCloser per continuar.
									</p>
									<div class="form-action">
										<label for="txt_name">
											<input id="userName" name="userName" class="txt fill-width" type="text" placeholder="Enter full name"/>
											<span id="nomError" class="errorInfo"></span>
										</label>
										<div class="wrap-2col clearfix">
											<div class="col">
												<label for="txt_email">
													<input id="userEmail" name="userEmail" class="txt fill-width" type="email" placeholder="Enter your e-mail"/>
													<span id="emailError" class="errorInfo"></span>
												</label>
												<label for="txt_re_email">
													<input id="userEmail_confirm" name="userEmail_confirm" class="txt fill-width" type="email" placeholder="Re-enter your e-mail"/>
												</label>
											</div>
											<div class="col">
												<label for="txt_password">
													<input id="userPassword" name="userPassword" class="txt fill-width" type="password" placeholder="Enter password"/>
													<span id="contrasenyaError" class="errorInfo"></span>
												</label>
												<label for="txt_re_password">
													<input id="userPassword_confirm" name="userPassword_confirm" class="txt fill-width" type="password" placeholder="Re-enter password"/>
												</label>
											</div>
										</div>
										<p class="rs pb10">
											En registrar-se, vostè està d'acord amb la nostra
											<a href="#" class="fc-orange">
												condicions d'ús
											</a> 
											i 
											<a href="#" class="fc-orange">
												política de privacitat
											</a>.
										</p>
										<p class="rs ta-c">
											<button id="btn-register" class="btn btn-red btn-submit" name="btn-register" type="submit">
												<span class="glyphicon glyphicon-log-in"></span> &nbsp; Registra't
											</button>
										</p>
									</div>
								</div>
							</form>
						</div>
						</div><!-- /.col-md-6 -->
						<div class="col-xs-12 col-md-6">
						<div class="form login-form">
							<form class="form-signin" method="post" id="login-form">
								<h3 class="rs title-form">Entrar</h3>
								<div class="box-white">
									<div id="errorLogin">
									<!-- error will be shown here ! -->
									</div>
									<h4 class="rs title-box">
										Ja tens un compte?
									</h4>
									<p class="rs">
										Si us plau entra per continuar.
									</p>
									<div class="form-action">
										<label for="txt_email_login">
											<input class="txt fill-width" type="email" placeholder="Email address" name="user_email" id="user_email" />
										</label>
										<label for="txt_password_login">
											<input class="txt fill-width" type="password" placeholder="Password" name="password" id="password" />
										</label>
										<p class="rs ta-c pb10"> 
											<button class="btn btn-red btn-submit" name="btn-login" id="btn-login" type="submit">
												<span class="glyphicon glyphicon-log-in"></span> &nbsp; Entrar
											</button>
										</p>
										<p class="rs ta-c">
											<a href="#" class="fc-orange">
												He oblidat la meva contrasenya
											</a>
										</p>
									</div>
								</div>
							</form>
						</div>
					</div><!-- /.col-md-6 -->
				</div>
			</div>
			<?php include 'assets/blocks_includes/footer.php'; ?>

		</div>

		<?php 
			include 'assets/blocks_includes/registrar_login.php';
			include 'assets/blocks_includes/scripts_footer.php';
		?>
	</body>

	<!-- Mirrored from envato.megadrupal.com/html/kickstars/category.html by HTTrack Website Copier/3.x [XR&CO'2013], Thu, 06 Jun 2013 09:23:52 GMT -->
</html>