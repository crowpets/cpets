-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-05-2016 a las 19:28:06
-- Versión del servidor: 5.6.26
-- Versión de PHP: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `crowdfunding`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `project`
--

CREATE TABLE IF NOT EXISTS `project` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `categoria` varchar(45) NOT NULL,
  `titol` varchar(45) NOT NULL,
  `pais` varchar(45) NOT NULL,
  `ciutat` varchar(45) NOT NULL,
  `multimedia` varchar(255) NOT NULL,
  `termini` int(11) NOT NULL,
  `cost_projecte` float NOT NULL,
  `data_creacio` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `historia` varchar(1000) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `project`
--

INSERT INTO `project` (`id`, `user_id`, `categoria`, `titol`, `pais`, `ciutat`, `multimedia`, `termini`, `cost_projecte`, `data_creacio`, `historia`) VALUES
(6, 25, 'tecnologies', 'estitol', 'ES', 'esciutat', '0000f5f4_medium.jpeg', 33, 111, '2016-05-02 16:12:35', 'Lorem Ipsum es simpl de    lera que logró hacer un libroe textos esnLorem Ipsum es simpl de    lera que logró hacer un libroe textos esnLorem Ipsum es simpl de    lera que logró hacer un libroe textos esnLorem Ipsum es simpl de    lera que logró hacer un libroe textos esn'),
(7, 25, 'tecnologies', 'estitol', 'ES', 'esciutat', '0000f5f4_medium.jpeg', 33, 222, '2016-05-02 16:12:40', 'Lorem Ipsum es simplemente el texto de relleno de rchivos de texto. Lorem sidoLorem Ipsum es simplemente el texto de relleno de rchivos de texto. Lorem sidoLorem Ipsum es simplemente el texto de relleno de rchivos de texto. Lorem sidoLorem Ipsum es simplemente el texto de relleno de rchivos de texto. Lorem sido'),
(16, 26, 'tecnologies', 'Ajuda els animals', 'ES', 'REUS', '0000f5f4_medium.jpeg', 33, 333, '2016-05-02 16:12:43', 'Lorem Ipsum es simplemente el texto de relleno de rchivos de texto. Lorem sidoLorem Ipsum es simplemente el texto de relleno de rchivos de texto. Lorem sidoLorem Ipsum es simplemente el texto de relleno de rchivos de texto. Lorem sidoLorem Ipsum es simplemente el texto de relleno de rchivos de texto. Lorem sidoLorem Ipsum es simplemente el texto de relleno de rchivos de texto. Lorem sido'),
(17, 26, 'tecnologies', 'Ajuda els animals', 'ES', 'REUS', '0000f5f4_medium.jpeg', 33, 333, '2016-05-02 16:12:50', 'Lorem Ipsum es simplemente el texto de relleno de rchivos de texto. Lorem sidoLorem Ipsum es simplemente el texto de relleno de rchivos de texto. Lorem sidoLorem Ipsum es simplemente el texto de relleno de rchivos de texto. Lorem sidoLorem Ipsum es simplemente el texto de relleno de rchivos de texto. Lorem sidoLorem Ipsum es simplemente el texto de relleno de rchivos de texto. Lorem sidoel texto de rellerchivos de texto.Loremum es simtextorellenodelasimimprentas ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recompenses`
--

CREATE TABLE IF NOT EXISTS `recompenses` (
  `id_recompensa` int(11) NOT NULL,
  `id_projecte` int(11) NOT NULL,
  `contribucio` int(11) NOT NULL,
  `descripcio` varchar(300) NOT NULL,
  `entrega_estimada` date NOT NULL,
  `patrocinador` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `recompenses`
--

INSERT INTO `recompenses` (`id_recompensa`, `id_projecte`, `contribucio`, `descripcio`, `entrega_estimada`, `patrocinador`) VALUES
(6, 6, 33, 'sdfdsfdsfsdfd,s,fmdslmgklfmg.dfm', '2016-05-20', 9),
(7, 7, 33, 'mflkdklmfklmlfkdmklfmsdklmfkldsmfkldsmflkmsdklfmdslkmfkldmfklsd', '2016-05-27', 33),
(8, 16, 33, 'Aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', '2016-05-31', 33),
(9, 17, 33, 'Aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', '2016-05-31', 33);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(25) NOT NULL,
  `user_email` varchar(60) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `joining_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `user_email`, `user_password`, `joining_date`) VALUES
(1, 'prova', 'prova@gmail.com', '1111', '2015-11-08 16:25:19'),
(2, 'test', 'test@test.test', '1111', '2015-11-14 12:37:19'),
(23, 'aa', 'aa@aa.aa', 'aaaaa', '2016-04-16 10:17:56'),
(24, 'demo', 'demo@demo.com', 'demodemo', '2016-04-16 11:05:13'),
(25, 'eloi colom salvans', 'email@email.email', 'email@email.email', '2016-05-02 13:53:58'),
(26, 'Zhuwei', 'zjiang@gmail.com', 'admin', '2016-05-02 16:04:18');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_user_id_idx` (`user_id`);

--
-- Indices de la tabla `recompenses`
--
ALTER TABLE `recompenses`
  ADD PRIMARY KEY (`id_recompensa`),
  ADD KEY `id_projecte` (`id_projecte`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `project`
--
ALTER TABLE `project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT de la tabla `recompenses`
--
ALTER TABLE `recompenses`
  MODIFY `id_recompensa` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `project`
--
ALTER TABLE `project`
  ADD CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `recompenses`
--
ALTER TABLE `recompenses`
  ADD CONSTRAINT `recompenses_ibfk_1` FOREIGN KEY (`id_projecte`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
