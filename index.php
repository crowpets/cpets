<?php
session_start();
?>
<!DOCTYPE html>
<html>

	<!-- Mirrored from envato.megadrupal.com/html/kickstars/ by HTTrack Website Copier/3.x [XR&CO'2013], Thu, 06 Jun 2013 09:21:02 GMT -->
	<head>
		<title>Pet Closer</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no" />
		<?php include 'assets/blocks_includes/links/links_index.php'; ?>
		<?php include 'assets/blocks_includes/links_head.php'; ?>
		<?php include 'assets/project/get_projects.php'; ?>
		
	</head>
	<body>
		<div id="wrapper">
			<?php include 'assets/blocks_includes/header.php'; ?>
			<div id="home-slider">
				<div class="md-slide-items md-slider" id="md-slider-1" data-thumb-width="105" data-thumb-height="70">
					<div class="md-slide-item slide-0" data-timeout="6000">
						<div class="md-mainimg"><img src="assets/images/ex/s1.jpg" alt="">
						</div>
						<div class="md-objects">
							<div class="md-object slide-with-background" data-x="10" data-y="58" data-width="500" data-height="170" data-padding-top="30" data-padding-bottom="30" data-padding-left="35" data-padding-right="35" data-start="300" data-stop="3600" data-easein="random" data-easeout="keep">
								<h2 class="rs slide-title">Ajuda els animals</h2>
							</div>
							<div class="md-object" data-x="800" data-y="0" data-width="300" data-height="300" data-start="1800" data-stop="7500" data-easein="fadeInRight" data-easeout="keep" style=""><img src="assets/images/ex/p1.png" alt="Search Money for Your Creative Ideas" width="612" height="365" />
							</div>
						</div>
					</div>
					<div class="md-slide-item slide-1" data-timeout="6000">
						<div class="md-mainimg"><img src="assets/images/ex/th-slide1.jpg" alt="">
						</div>
						<div class="md-objects">
							<div class="md-object slide-with-background" data-x="1" data-y="58" data-width="500" data-height="170" data-padding-top="30" data-padding-bottom="30" data-padding-left="35" data-padding-right="35" data-start="300" data-stop="3600" data-easein="random" data-easeout="keep">
								<p class="rs slide-title">
									Tus idees per crear noves gadgets
								</p>
							</div>
							<div class="md-object" data-x="454" data-y="44" data-width="327" data-height="268" data-start="1000" data-stop="5500" data-easein="random" data-easeout="random" style=""><img src="assets/images/ex/p2.png" alt="Responsive" width="327" height="268" />
							</div>
							<div class="md-object" data-x="828" data-y="142" data-width="298" data-height="176" data-start="1600" data-stop="5100" data-easein="random" data-easeout="random" style=""><img src="assets/images/ex/p21.jpg" alt="Responsive" width="298" height="176" />
							</div>
							<div class="md-object" data-x="750" data-y="200" data-width="119" data-height="149" data-start="2200" data-stop="4800" data-easein="random" data-easeout="random" style=""><img src="assets/images/ex/p22.jpg" alt="Responsive" width="119" height="149" />
							</div>

						</div>
					</div>
					<div class="md-slide-item slide-2" data-timeout="4000">
						<div class="md-mainimg"><img src="assets/images/ex/s31.jpg" alt="">
						</div>
						<div class="md-objects">
							<div class="md-object slide-with-background" data-x="20" data-y="58" data-width="500" data-height="170" data-padding-top="30" data-padding-bottom="30" data-padding-left="35" data-padding-right="35" data-start="300" data-stop="3600" data-easein="random" data-easeout="keep">
								<h2 class="rs slide-title">Començes tu projecte avui!</h2>
							</div>
						</div>
					</div>
				</div>
			</div><!--end: #home-slider -->
			<br>
			<br>
			<br>
			<div id="portfolio" class="bg-light-gray">
				<div class="container">
					<div class="row">
						<div class="grid_12 wrap-title">
							<h2 class="common-title">Pasos</h2>
						</div>
					</div>
					<div class="row text-center">
						<div class="col-sm-4">
							<img class="img-circle img-responsive img-centered" src="assets/images/ex/pas1.jpg" alt="">
							<h2>Pensa una idea</h2>
						</div>
						<div class="col-sm-4">
							<img class="img-circle img-responsive img-centered" src="assets/images/ex/pas2.jpg" alt="">
							<h2>Recapta els diners</h2>
						</div>
						<div class="col-sm-4">
							<img class="img-circle img-responsive img-centered" src="assets/images/ex/pas3.jpg" alt="">
							<h2>Compleix el teu somni</h2>
						</div>
					</div>
				</div>
			</div>
			<!--end: #image -->
			<!-- Portfolio Grid Section -->
			<div id="portfolio" class="bg-light-gray">
				<div class="container">
					<div class="row">
						<div class="grid_12 wrap-title">
							<h2 class="common-title">Categories</h2>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4 col-sm-6 portfolio-item">
							<a href="categories.php?cat=tecnologies" class="portfolio-link" data-toggle="modal">
							<div class="portfolio-hover">
								<div class="portfolio-hover-content">
									<i class="fa fa-plus fa-3x"></i>
								</div>
							</div> <img src="assets/images/ex/cat1.jpg" class="img-responsive" alt=""> </a>
							<div class="portfolio-caption">
								<h4>Tecnològies</h4>
								<p class="text-muted">
									Noves tecnològies
								</p>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 portfolio-item">
							<a href="categories.php?cat=disseny" class="portfolio-link" data-toggle="modal">
							<div class="portfolio-hover">
								<div class="portfolio-hover-content">
									<i class="fa fa-plus fa-3x"></i>
								</div>
							</div> <img src="assets/images/ex/cat2.png" class="img-responsive" alt=""> </a>
							<div class="portfolio-caption">
								<h4>Disseny</h4>
								<p class="text-muted">
									Estils
								</p>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 portfolio-item">
							<a href="categories.php?cat=ajuda" class="portfolio-link" data-toggle="modal">
							<div class="portfolio-hover">
								<div class="portfolio-hover-content">
									<i class="fa fa-plus fa-3x"></i>
								</div>
							</div> <img src="assets/images/ex/cat3.jpg" class="img-responsive" alt=""> </a>
							<div class="portfolio-caption">
								<h4>Ajuda</h4>
								<p class="text-muted">
									Donació
								</p>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
		<div class="home-popular-project">
			<div class="container_12">
				<div class="grid_12 wrap-title">
					<h2 class="common-title">Popular</h2>
				</div>
				<div class="clear"></div>
				<div class="lst-popular-project clearfix">
					<?php
					$projects = getProjectAll($db_con);
					foreach ($projects as $row) {
						echo '<div class="grid_3">
						<div class="project-short sml-thumb">
							<div class="top-project-info">
								<div class="content-info-short clearfix">
									<a href="project.php?project_id='.$row["id"].'" class="thumb-img"> <img src="uploads_users/'.$row["multimedia"].'" alt="$TITLE"> </a>
									<div class="wrap-short-detail">
										<h3 class="rs acticle-title"><a class="be-fc-orange" href="project.php">'.$row["titol"].'</a></h3>
										<p class="rs tiny-desc">
											by <a href="profile.html" class="fw-b fc-gray be-fc-orange">'.$row["user_name"].'</a>
										</p>
										<p class="rs title-description">
											'.mb_strimwidth($row["historia"], 0, 200, "...").'
										</p>
										<p class="rs project-location">
											<i class="icon iLocation"></i>
											'.$row["ciutat"].'
										</p>
									</div>
								</div>
							</div>
							<div class="bottom-project-info clearfix">
								<div class="line-progress">
									<div class="bg-progress">
										<span  style="width: 50%"></span>
									</div>
								</div>
								<div class="group-fee clearfix">
									<div class="fee-item">
										<p class="rs lbl">
											Funded
										</p>
										<span class="val">50%</span>
									</div>
									<div class="sep"></div>
									<div class="fee-item">
										<p class="rs lbl">
											Pledged
										</p>
										<span class="val">$38,000</span>
									</div>
									<div class="sep"></div>
									<div class="fee-item">
										<p class="rs lbl">
											Days Left
										</p>
										<span class="val">25</span>
									</div>
								</div>
							</div>
						</div>
					</div><!--end: .grid_3 > .project-short-->';
					}
				?>
				</div>
			</div>
		</div><!--end: .home-popular-project -->

		<?php include 'assets/blocks_includes/footer.php'; ?>

		</div>

		<?php 
			include 'assets/blocks_includes/registrar_login.php';
			include 'assets/blocks_includes/scripts_footer.php';
		?>
		
	</body>

	<!-- Mirrored from envato.megadrupal.com/html/kickstars/ by HTTrack Website Copier/3.x [XR&CO'2013], Thu, 06 Jun 2013 09:23:06 GMT -->
</html>
