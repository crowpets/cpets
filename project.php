<?php
session_start();
?>
<!DOCTYPE html>
<html>

	<!-- Mirrored from envato.megadrupal.com/html/kickstars/project.html by HTTrack Website Copier/3.x [XR&CO'2013], Thu, 06 Jun 2013 09:23:52 GMT -->
	<head>
		<title>Projecte</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no" />
		<?php include 'assets/blocks_includes/links_head.php'; ?>
		<?php include 'assets/blocks_includes/links/links_projecte.php'; ?>
		<?php include 'assets/project/get_projects.php'; ?>
	</head>
	<body>

		<div id="wrapper">
			<?php include 'assets/blocks_includes/header.php'; ?>
			<?php
			$id_project = $_GET["project_id"];
			$project = getProject($db_con, $id_project);
			echo '<div class="layout-2cols">
				<div class="content grid_8">
					<div class="project-detail">
						<h2 class="rs project-title">'.$project["titol"].'</h2>
						<p class="rs post-by">
							by <a href="#">'.$project["user_name"].'</a>
						</p>
						<div class="project-short big-thumb">
							<div class="top-project-info">
								<div class="content-info-short clearfix">
									<div class="thumb-img">
										<div class="rslides_container">
											<ul class="rslides" id="slider1">
												<li><img src="uploads_users/'.$project["multimedia"].'" alt="">
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div><!--end: .top-project-info -->
							
						</div>
						<div class="project-tab-detail tabbable accordion">
							<ul class="nav nav-tabs clearfix">
								<li class="active">
									<a href="#">Història</a>
								</li>
								<!--li>
									<a href="#" class="be-fc-orange">Backers (270)</a>
								</li>
								<li>
									<a href="#" class="be-fc-orange">Comentaris (2)</a>
								</li-->
							</ul>
							<div class="tab-content">
								<div>
									<h3 class="rs alternate-tab accordion-label">About</h3>
									<div class="tab-pane active accordion-content">
										<div class="editor-content">
											<h3 class="rs title-inside">'.$project["titol"].'</h3>
											<p class="rs post-by">
												by <a href="#" class="fw-b fc-gray be-fc-orange">'.$project["user_name"].'</a> en <span class="fw-b fc-gray">'.$project["ciutat"].'</span>
											</p>
											<p>
												'.$project["historia"].'
											</p>							
											<p>
												<img class="img-desc" src="uploads_users/'.$project["multimedia"].'" alt="$DESCRIPTION"/>
							
											</p>
								
											<div class="social-sharing">
												<!-- AddThis Button BEGIN -->
												<div class="addthis_toolbox addthis_default_style">
													<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
													<a class="addthis_button_tweet"></a>
													<a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
													<a class="addthis_counter addthis_pill_style"></a>
												</div>
												<script type="text/javascript" src="../../../s7.addthis.com/js/300/addthis_widget.js#pubid=undefined"></script>
												<!-- AddThis Button END -->
											</div>
										</div>
										<div class="project-btn-action">
											<a class="btn big btn-red" href="#">Realitzar una pregunta</a>
											<a class="btn big btn-black" href="#">Compartir el projecte</a>
										</div>
									</div><!--end: .tab-pane(About) -->
								</div>
							</div>
						</div><!--end: .project-tab-detail -->
					</div>
				</div><!--end: .content -->
				<div class="sidebar grid_4">
					<div class="project-runtime">
						<div class="box-gray">
							<div class="project-date clearfix">
								<span class="val"><span class="fw-a">Llançament: </span>'.$project["data_creacio"].'</span>
							</div>
							<div class="project-date clearfix">
								<span class="val"><span class="fw-a">Fin: </span>31 de decembre, 2016</span>
							</div>
							<div class="project-date clearfix">
								<span class="val"><span class="fw-a">Patrocinadors: </span>111</span>
							</div>
							<div class="project-date clearfix">
								<span class="val"><span class="fw-a">Contribuït: </span>1000€</span>
							</div>
							<div class="project-date clearfix">
								<span class="val"><span class="fw-a">Queden: </span>365 dies</span>
							</div>
							<a class="btn btn-green btn-buck-project" href="#"> <span class="lbl">Contribuir ara</span> </a>
							<p class="rs description">
								This project will only be funded if at least $15,000 is pledged by Wednesday May 8, 2:05pm EDT.
							</p>
						</div>
					</div><!--end: .project-runtime -->
					<div class="clear clear-2col"></div>
					<div class="wrap-nav-pledge">
						<ul class="rs nav nav-pledge accordion">';
						$recompenses = getRecompenses($db_con, $id_project);
							foreach ($recompenses as $row) {
								echo '<li>
								<div class=" pledge-label accordion-label clearfix">
									<i class="icon iPlugGray"></i>
									<span class="pledge-amount">Contribuir '.$row["contribucio"].'€ o més</span>
				
								</div>
								<div class=" pledge-content accordion-content">
									<div class="pledge-detail">
										<p class="rs pledge-description">
											'.mb_strimwidth($row["descripcio"], 0, 40, "...").'
										</p>
										<p class="rs">
											<span class="fw-b">Entrega estimada:</span> '.$row["entrega_estimada"].'
										</p>
										<br>
										<p class="rs ta-c">
										<form name="formTpv" method="post" action="https://www.sandbox.paypal.com/cgi-bin/webscr">
											<input type="hidden" name="cmd" value="_xclick">
											<input type="hidden" name="business" value="zjiang1991iam@gmail.com">
											<input type="hidden" name="item_name" value="'.$row["descripcio"].'">
											<!--input type="hidden" name="item_number" value="Articulo 1"-->
											<input type="hidden" name="amount" value="'.$row["contribucio"].'">
											<input class="btn big btn-red" type="submit" value="Contribuir ara"/>
										</form>
										</p>
			
									</div>
								</div>
							</li>';
								}	
			echo	'</ul>
					</div><!--end: .wrap-nav-pledge -->
				</div><!--end: .sidebar -->
				<div class="clear"></div>
			</div>';
			?>
			<?php include 'assets/blocks_includes/footer.php'; ?>

		</div>

		<?php 
			include 'assets/blocks_includes/registrar_login.php';
			include 'assets/blocks_includes/scripts_footer.php';
		?>
	</body>

	<!-- Mirrored from envato.megadrupal.com/html/kickstars/project.html by HTTrack Website Copier/3.x [XR&CO'2013], Thu, 06 Jun 2013 09:24:22 GMT -->
</html>